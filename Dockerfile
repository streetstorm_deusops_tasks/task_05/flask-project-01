FROM python:3.10.8-slim-bullseye

ENV PYTHONDONTWRITEBYTECODE 1 \
    PYTHONUNBUFFERED 1

WORKDIR /python-docker

COPY requirements* ./
RUN apt update && \
    apt install -y python3-dev default-libmysqlclient-dev build-essential && \
    rm -rf /var/lib/apt/lists/* && \
    pip3 install -r requirements.txt && \
    pip3 install -r requirements-server.txt

COPY . /python-docker/

EXPOSE 8000/tcp

CMD ["/bin/sh", "-c", "flask db upgrade && gunicorn app:app -b 0.0.0.0:8000"]
